Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    resources :login, :nominations
    
    root "login#index"
     
    #get "/all-nominations", to: "nomination#index"
    #get "/nominations/new", to: "nomination#new"
end
