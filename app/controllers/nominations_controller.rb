require 'json'
class NominationsController < ApplicationController
       
    def index
        json_response  = File.read("#{Rails.root}/mockJson.json")
        hash_response  = JSON.parse(json_response )
        @nominations = hash_response         
        respond_to do |format|
           format.html  
           format.json  { render :json => @nominations }
        end
    end
    
   def new
        @nomination = ""
        @status = params[:status]
   end
    
                                 
    def show
        @window = params[:id]
        if(@window == "notification")
            json_response  = File.read("#{Rails.root}/mockJsonNotify.json")
            hash_response  = JSON.parse(json_response )
            @notifications = hash_response         
            respond_to do |format|
               format.html  {render "nominations/notification"}
               format.json  { render :json => @notifications }
            end
        elsif(@window == "schedule")
            json_response  = File.read("#{Rails.root}/mockJsonSchedule.json")
            hash_response  = JSON.parse(json_response )
            @schedules = hash_response   
            respond_to do |format|
               format.html  {render "nominations/schedule"}
               format.json  { render :json => @schedules }
            end
        end
     end
    
    def create
        nomination_list = [params[:nominationstartdate], params[:nominationenddate], params[:injectionpoint], params[:injectionquantity], params[:offtakepoint], params[:offtakequantity], params[:capacity]]
        #have to make API call
        redirect_to :controller => "nominations", :action => "index" 
    end       
end
